void insert_front(struct Node** head, int data)
{
    struct Node* newNode = NULL;
    try
    {
        newNode = new Node;
        newNode->data = data;
    }
    catch (...)
    {
        cout << "No hay suficiente memoria";
        exit(0);
    }

   if ((*head) != NULL)
   {
        newNode->next = (*head);
        newNode->prev = NULL;
        (*head)->prev = newNode;
   }
   else
   {
        newNode->next = NULL;
        newNode->prev = NULL;
   }

   (*head) = newNode;
}
