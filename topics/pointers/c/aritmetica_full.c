
#include <stdio.h>
int main() {
    // Declaración y asignacion de tres variables de diferentes tipos
    int a = 10;
    float b = 20.5;
    char c = 'Z';

    // Declaración de tres punteros y asignación de las direcciones de memoria de las variables
    int *ptrA = &a;
    float *ptrB = &b;
    char *ptrC = &c;

    // Imprimir las direcciones de memoria y el contenido de cada puntero
    printf("Dirección de memoria de a: %p, Contenido: %d\n", ptrA, *ptrA);
    printf("Dirección de memoria de b: %p, Contenido: %.2f\n", ptrB, *ptrB);
    printf("Dirección de memoria de c: %p, Contenido: %c\n", ptrC, *ptrC);

    // Incrementar en una unidad cada puntero
    ptrA++;
    ptrB++;
    ptrC++;

    // Imprimir nuevamente las direcciones de memoria y el contenido de cada puntero
    printf("\nDespués de incrementar los punteros:\n");
    printf("Dirección de memoria de a: %p\n",ptrA);
    printf("Dirección de memoria de b: %p\n",ptrB);
    printf("Dirección de memoria de c: %p\n",ptrC);

    return 0;
}
