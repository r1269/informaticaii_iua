#include <stdio.h>

void acum_by_reference(int *p);

int main() 
{   int dato = 3;
    int dato2 = 4;
    int *pointer_to_int = NULL;
    acum_by_reference(&dato);
    printf("Valor: %d\n", dato);
    pointer_to_int = &dato;
    acum_by_reference(pointer_to_int);
    printf("Valor: %d\n", dato);
    return 0;
}

void acum_by_reference(int *p)
{
    *(p) = *(p) + 1;
}
