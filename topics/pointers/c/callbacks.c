#include <stdio.h>
// Funciones de operaciones matematicas
int suma(int a, int b) {
    return a + b;
}

int resta(int a, int b) {
    return a - b;
}

// Funcion que recibe un puntero a funcion como argumento
void operacion(int (*pop)(int, int), int x, int y) {
    int resultado = pop(x, y);
    printf("Resultado: %d\n", resultado);
}

int main() {
    int a = 10, b = 5;

    printf("Suma:\n");
    operacion(suma, a, b);

    printf("Resta:\n");
    operacion(resta, a, b);

    return 0;
}
