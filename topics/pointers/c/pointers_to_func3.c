#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
// Prototipos de funciones para diferentes operaciones
int sumar      (int a, int b);
int restar     (int a, int b);
int multiplicar(int a, int b);
int dividir    (int a, int b);

int main() {
    int a, b;
    uint8_t opcion;
    uint8_t valor;
    valor = 10;
    // Declaración del puntero a función
    int (*operacion)(int a, int b);
    
    printf("Ingresa dos números: ");
    scanf("%d %d", &a, &b);
    if(valor>0)
    {
        opcion=1;
    }
    
    // Asignar la función apropiada según la elección del usuario
    switch (opcion) {
        case 1:
            operacion = sumar;
            break;
        case 2:
            operacion = restar;
            break;
        case 3:
            operacion = multiplicar;
            break;
        case 4:
            operacion = dividir;
            break;
        default:
            printf("Elección inválida\n");
            return 1;
    }
    
    // Realizar la operación seleccionada usando el puntero a función
    int resultado;
    resultado = operacion(a, b);
    printf("Resultado: %d\n", resultado);
    
    return 0;
}

// Definición de diferentes operaciones
int sumar(int a, int b) {
    return a + b;
}

int restar(int a, int b) {
    return a - b;
}

int multiplicar(int a, int b) {
    return a * b;
}

int dividir(int a, int b) {
    if (b != 0) {
        return a / b;
    } else {
        printf("Error: División por cero\n");
        return 0;
    }
}
