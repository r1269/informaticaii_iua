#include <stdio.h>

void loadMatrix(int* matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("Ingrese la posicion[%d][%d]: ", i, j);
            scanf("%d", matrix + i * cols + j);
        }
    }
}

void printMatrix(const int* matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d\t", *(matrix + i * cols + j));
        }
        printf("\n");
    }
}

int main() {
    int matrix[3][6];

    printf("Ingrese valores\n");
    loadMatrix(&matrix[0][0], 3, 6);

    printf("\nImpresion:\n");
    printMatrix(&matrix[0][0], 3, 6);

    

    return 0;
}
