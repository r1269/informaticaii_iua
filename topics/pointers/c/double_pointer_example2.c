#include <stdio.h>

void change_pointer(int **p, int *new_data);

int main() 
{
  int dato1 = 3;
  int dato2 = 2;
  int *pint = NULL;
  pint = &dato2;
  printf("Direccion dato1: %p\n", &dato1);
  printf("Direccion dato2: %p\n", &dato2);
  printf("Direccion de pint: %p\n", &pint);
  printf("Valor de pint: %p\n", pint);
  printf("Valor apuntado por pint: %d\n", *(pint));
  change_pointer(&pint, &dato2);
  printf("Direccion dato1: %p\n", &dato1);
  printf("Direccion dato2: %p\n", &dato2);
  printf("Direccion de pint: %p\n", &pint);
  printf("Valor de pint: %p\n", pint);
  printf("Valor apuntado por pint: %d\n", *(pint));
  change_pointer(&pint, &dato1);
  printf("Direccion dato1: %p\n", &dato1);
  printf("Direccion dato2: %p\n", &dato2);
  printf("Direccion de pint: %p\n", &pint);
  printf("Valor de pint: %p\n", pint);
  printf("Valor apuntado por pint: %d\n", *(pint));
  change_pointer(&pint, &dato2);
  printf("Direccion dato1: %p\n", &dato1);
  printf("Direccion dato2: %p\n", &dato2);
  printf("Direccion de pint: %p\n", &pint);
  printf("Valor de pint: %p\n", pint);
  printf("Valor apuntado por pint: %d\n", *(pint));
  return 0;
}

void change_pointer(int **p, int *new_data)
{
    *(p) = new_data;
}
