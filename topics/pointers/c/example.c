#include <stdlib.h>
#include <stdint.h>


void R_16_1 ( void )
{
    uint8_t zwitch = get_uint8 ( );
    uint8_t zwitched = get_uint8 ( );

    switch ( zwitch )
    {
       //uint8_t decl;   /* Non-compliant */
       case 0:
       {
          zwitched += 1;
          decl = zwitched;
          use_uint8 ( decl );
          break;
       }
       case 1:
       {
          uint8_t local = zwitched;
          use_uint8 ( local );
          break;
       }
       case 2:
         use_uint8 ( zwitched );
         break;
       default:
       {
          break;
       }
    }

    use_uint8 ( zwitched );
}

