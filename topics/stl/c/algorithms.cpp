#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main()
{   int ii;
    vector<int> vect(10);
    for(ii=0;ii<vect.size();ii++)    
        vect[ii]=rand();
    cout << "Vector: "<<endl; 
    for(ii=0;ii<vect.size();ii++)    
        cout<< " "<<vect[ii]<<" "<<endl;

    //Ordenamiento ascendente
    sort(vect.begin(), vect.end());

    cout << "Vector luego del ordenamiento: "<<endl;
    for(ii=0;ii<vect.size();ii++)    
       cout << vect[ii] << " "<<endl;

    //Ordenamiento descendente
    sort(vect.begin(),vect.end(), greater<int>());

    cout << "Vector luego del ordenamiento desc: "<<endl;
    for(ii=0;ii<vect.size();ii++)    
       cout << vect[ii]<< " "<<endl;
    
    //Inversiin del arreglo
    reverse(vect.begin(), vect.end());
    cout << "\nVector invertido "<<endl;
    for(ii=0;ii<vect.size();ii++)    
        cout << vect[ii]<< " "<<endl;

    cout << "\nBusqueda de max y minimo "<<endl;
    cout << *max_element(vect.begin(), vect.end())<<endl;
    cout << *min_element(vect.begin(), vect.end())<<endl;
 
    return 0;
}