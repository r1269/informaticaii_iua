#include <iostream>
#include <vector>
#include <cctype>
using namespace std;
int main (void)
{
    int ii;
    vector<int> v(10);
    cout<< "Tam de v: "<< v.size()<<endl;
    //asignando valores
    for(ii=0;ii<v.size();ii++)
        v[ii]=ii;
    //Recorriendo
    for(ii=0;ii<v.size();ii++)    
        cout<< " "<<v[ii]<<" "<<endl;
    //Agregando al final
    for(ii=0;ii<5;ii++)    
        v.push_back(10+ii);
    
    cout<< "Nuevo tam de v: "<< v.size()<<endl;
    //Recorriendo
    for(ii=0;ii<v.size();ii++)    
        cout<< " "<<v[ii]<<" "<<endl;
    return(0);
}
