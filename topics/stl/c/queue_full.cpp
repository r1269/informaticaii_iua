#include <iostream>
#include <queue>
using namespace std;
  
void print_queue(queue<int>);



int main()
{
    queue<int> q;
    q.push(10);
    q.push(20);
    q.push(30);
    cout << "Impresion: ";
    print_queue(q);
    cout << "q.size()  : "  << q.size()  <<endl;
    cout << "q.front() : " << q.front()<<endl;
    cout << "q.back()  : "  << q.back()  <<endl;
    q.pop();
    print_queue(q);
    return 0;
}



void print_queue(queue<int> q)
{
    queue<int> copy_queue=q;
    while (!copy_queue.empty()) 
    {
        cout << '\t' << copy_queue.front();
        copy_queue.pop();
    }
    cout << '\n';
}
