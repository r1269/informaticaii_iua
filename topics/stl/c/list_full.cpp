#include <iostream>
#include <iterator>
#include <list>
using namespace std;
  
void print_lst(list<int> );



int main()
{
  
    list<int> l1, l2;
  
    for (int i = 0; i < 10; ++i) 
    {
        l1.push_back(i);
        l2.push_front(i*2);
    }
    cout << "l1:"<<endl;
    print_lst(l1);
    
    cout << "l2:"<<endl;
    print_lst(l2);
  
    cout << "l1 front: " << l1.front()<<endl;
    cout << "l1 back: " << l1.back()<<endl;
    l1.pop_front();
    cout << "l1:"<<endl;
    print_lst(l1);
    cout << "l2:"<<endl;
    l2.pop_back();
    print_lst(l2);
    l1.merge(l2);
    cout << "l1 merged:"<<endl;
    print_lst(l1);

    return 0;
}

void print_lst(list<int> l)
{
    list<int>::iterator it;
    for (it = l.begin(); it != l.end(); ++it)
        cout << '\t' << *(it);
    cout << '\n';
}
