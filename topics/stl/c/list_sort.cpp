#include <iostream>
#include <iterator>
#include <list>
#include <cstdlib>
using namespace std;
  
void print_lst(list<int> );








int main()
{
    list<int> l1;
    for (int i = 0; i < 50; ++i) 
    {
        l1.push_front(rand());
    }
    cout << "l1:"<<endl;
    print_lst(l1);
    l1.sort();
    cout << "l1:"<<endl;
    print_lst(l1);
    return 0;
}

void print_lst(list<int> l)
{
    list<int>::iterator it;
    for (it = l.begin(); it != l.end(); ++it)
        cout <<*(it)<<'\n';
    cout << '\n';
}
