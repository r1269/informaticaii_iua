#include <iostream>
#include <vector>
#include <cctype>
using namespace std;

int main (void)
{
    int ii=0;
    vector<int> v(10);
    vector<int>::iterator p;
    
    p=v.begin();
    
    
    while(p!= v.end())
    {  *(p)=ii;
        ii++;
        p++;
    }
    p=v.begin();
    cout<< "Tam de v: "<< v.size()<<endl;
    v.insert(p+3,10);
    p=v.begin();
    cout<< "Tam de v: "<< v.size()<<endl;
    while(p!= v.end())
    {
        cout<< " "<< *(p) <<endl;
        p++;
    }
    p=v.begin();
    v.erase(p+3);
    cout<< "Tam de v: "<< v.size()<<endl;
    p=v.begin();
    while(p!= v.end())
    {
        cout<< " "<< *(p) <<endl;
        p++;
    }  
    return(0);
}
