#include <stdio.h>
#include <stdint.h>

// Definición de un identificador de mensajes CAN
typedef enum {
    ID_VELOCIDAD = 0x100,
    ID_DISTANCIA_OBSTACULO = 0x200,
    ID_DIRECCION_GIRO = 0x300
} CAN_ID;

// Estructura de un mensaje CAN estándar
typedef struct {
    uint16_t id;       // Identificador de mensaje CAN (11 bits en CAN estándar)
    uint8_t length;    // Longitud del mensaje (de 0 a 8 bytes)
    uint8_t data[8];   // Datos del mensaje (hasta 8 bytes)
} CAN_Message;

// Función para procesar mensajes CAN
void processCANMessage(const CAN_Message* msg) {
    switch (msg->id) {
        case ID_VELOCIDAD:
            {
                uint16_t velocidad = (msg->data[0] << 8) | msg->data[1]; // Velocidad en km/h
                printf("Velocidad del vehículo: %d km/h\n", velocidad);
            }
            break;
        
        case ID_DISTANCIA_OBSTACULO:
            {
                uint16_t distancia = (msg->data[0] << 8) | msg->data[1]; // Distancia en cm
                printf("Distancia al obstáculo: %d cm\n", distancia);
            }
            break;
        
        case ID_DIRECCION_GIRO:
            {
                int8_t angulo = msg->data[0]; // Ángulo de dirección (en grados)
                printf("Dirección de giro: %d grados\n", angulo);
            }
            break;
        
        default:
            printf("Mensaje CAN no reconocido. ID: 0x%X\n", msg->id);
            break;
    }
}

int main() {
    // Ejemplo de mensaje CAN de velocidad
    CAN_Message msgVelocidad = {ID_VELOCIDAD, 2, {0x03, 0xE8}};  // Velocidad: 1000 (0x03E8) km/h
    processCANMessage(&msgVelocidad);

    // Ejemplo de mensaje CAN de distancia a obstáculo
    CAN_Message msgDistancia = {ID_DISTANCIA_OBSTACULO, 2, {0x00, 0x64}};  // Distancia: 100 cm
    processCANMessage(&msgDistancia);

    // Ejemplo de mensaje CAN de dirección de giro
    CAN_Message msgDireccion = {ID_DIRECCION_GIRO, 1, {0x1E}};  // Dirección: 30 grados
    processCANMessage(&msgDireccion);

    return 0;
}

