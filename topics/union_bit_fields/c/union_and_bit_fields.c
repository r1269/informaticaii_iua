#include <stdio.h> 
 #include <stdint.h>
struct byte_bit{
    unsigned char bit_0 : 1;
    unsigned char bit_1 : 1;
    unsigned char bit_2 : 1;
    unsigned char bit_3 : 1;
    unsigned char bit_4 : 1;
    unsigned char bit_5 : 1;
    unsigned char bit_6 : 1;
    unsigned char bit_7 : 1;
};

union int_bits
{
    struct byte_bit bits_value;
    uint8_t  int_value;
};

typedef union int_bits int_to_bits;





int main()  
{  
  int_to_bits example;
  example.int_value=10;
  if(example.bits_value.bit_0)
    printf("El bit 0 esta en alto\n");
  else
    printf("El bit 0 esta en bajo\n");
  
  
  
  
  return(0);
}  