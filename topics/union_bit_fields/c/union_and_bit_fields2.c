#include<stdio.h>

struct ocho_bits 
{
 unsigned bit7:1;
 unsigned bit6:1;
 unsigned bit5:1;
 unsigned bit4:1;
 unsigned bit3:1;
 unsigned bit2:1;
 unsigned bit1:1;
 unsigned bit0:1;
};






union char_and_bits 
{
 unsigned char    data_char;
 struct ocho_bits bits_union;
};




int main(void)
{
    union char_and_bits test;
    test.data_char = 'a';
    printf("%d",test.bits_union.bit0);
    printf("%d",test.bits_union.bit1);
    printf("%d",test.bits_union.bit2);
    printf("%d",test.bits_union.bit3);
    printf("%d",test.bits_union.bit4);
    printf("%d",test.bits_union.bit5);
    printf("%d",test.bits_union.bit6);
    printf("%d",test.bits_union.bit7);
    
    printf("%c\n",test.data_char);
    test.bits_union.bit6=0x1;
    printf("%c\n",test.data_char);
    return(0);
}
