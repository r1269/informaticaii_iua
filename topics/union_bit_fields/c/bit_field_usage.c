#include <stdio.h> 


struct byte_bit{
    unsigned char bit_0 : 1;
    unsigned char bit_1 : 1;
    unsigned char bit_2 : 1;
    unsigned char bit_3 : 1;
    unsigned char bit_4 : 1;
    unsigned char bit_5 : 1;
    unsigned char bit_6 : 1;
    unsigned char bit_7 : 1;
};

typedef struct byte_bit byte_bit_t;

int main()  
{  
  byte_bit_t example;
  example.bit_0 = 1;
  example.bit_1 = 0;
  example.bit_2 = 1;

  if(example.bit_0)
    printf("1");
  else
    printf("0");
    
}  
