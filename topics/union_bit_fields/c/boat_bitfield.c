#include <stdio.h>
#include <stdint.h>

// Estructura que representa la posición y estado de un barco
typedef struct {
    // Coordenadas de posición en grados (simuladas como enteros por simplicidad)
    uint16_t latitud;            // 16 bits
    uint16_t longitud;           // 16 bits

    // Velocidad y rumbo
    uint8_t velocidad;           // en nudos (8 bits)
    uint8_t rumbo;               // en grados (8 bits)

    // Campos de bits para el estado del sistema
    unsigned sistema_navegacion : 1;   // 1 bit: 1 = activo, 0 = inactivo
    unsigned motor_principal    : 1;   // 1 bit: 1 = activo, 0 = inactivo
    unsigned motor_auxiliar     : 1;   // 1 bit: 1 = activo, 0 = inactivo
    unsigned sistema_radio      : 1;   // 1 bit: 1 = activo, 0 = inactivo
    unsigned falla_navegacion   : 1;   // 1 bit: 1 = falla, 0 = sin falla
    unsigned falla_motor        : 1;   // 1 bit: 1 = falla, 0 = sin falla
    unsigned falla_radio        : 1;   // 1 bit: 1 = falla, 0 = sin falla
    unsigned reserva            : 1;   // 1 bit de reserva
} EstadoBarco;

// Función para visualizar el estado del barco
void mostrarEstado(const EstadoBarco* barco) {
    printf("Posición: Latitud %d°, Longitud %d°\n", barco->latitud, barco->longitud);
    printf("Velocidad: %d nudos, Rumbo: %d°\n", barco->velocidad, barco->rumbo);
    printf("Sistemas:\n");
    printf("  Navegación: %s\n", barco->sistema_navegacion ? "Activo" : "Inactivo");
    printf("  Motor Principal: %s\n", barco->motor_principal ? "Activo" : "Inactivo");
    printf("  Motor Auxiliar: %s\n", barco->motor_auxiliar ? "Activo" : "Inactivo");
    printf("  Radio: %s\n", barco->sistema_radio ? "Activo" : "Inactivo");

    printf("Fallos:\n");
    printf("  Falla de Navegación: %s\n", barco->falla_navegacion ? "Sí" : "No");
    printf("  Falla en el Motor: %s\n", barco->falla_motor ? "Sí" : "No");
    printf("  Falla en el Sistema de Radio: %s\n", barco->falla_radio ? "Sí" : "No");
}

// Función para verificar si hay un fallo crítico
int falloCritico(const EstadoBarco* barco) {
    return barco->falla_navegacion || barco->falla_motor || barco->falla_radio;
}

int main() {
    // Estado simulado del barco
    EstadoBarco barco = {
        .latitud = 3456,           // Latitud simulada en grados (34.56°)
        .longitud = 7890,          // Longitud simulada en grados (78.90°)
        .velocidad = 12,           // 12 nudos
        .rumbo = 90,               // Rumbo hacia el este (90°)
        .sistema_navegacion = 1,   // Sistema de navegación activo
        .motor_principal = 1,      // Motor principal activo
        .motor_auxiliar = 1,       // Motor auxiliar activo
        .sistema_radio = 1,        // Sistema de radio activo
        .falla_navegacion = 0,     // Sin falla en navegación
        .falla_motor = 0,          // Sin falla en motor
        .falla_radio = 1           // Falla en el sistema de radio
    };

    // Mostrar el estado del barco
    mostrarEstado(&barco);

    // Verificar si hay fallos críticos
    if (falloCritico(&barco)) {
        printf("¡Advertencia: Se ha detectado un fallo crítico en el sistema!\n");
    } else {
        printf("Todos los sistemas están operativos.\n");
    }

    return 0;
}
