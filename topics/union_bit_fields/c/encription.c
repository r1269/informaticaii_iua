#include <stdio.h>
#include <string.h>

struct EncryptionData {
    char message[100];  // Campo para almacenar el mensaje
    char key[100];      // Campo para almacenar la clave
};

void encryptDecrypt(struct EncryptionData *data) {
    int messageLength = strlen(data->message);
    int keyLength = strlen(data->key);

    for (int i = 0; i < messageLength; ++i) {
        // Aplicar operación XOR entre el i-ésimo carácter del mensaje y el i-ésimo carácter de la clave
        data->message[i] = data->message[i] ^ data->key[i % keyLength];
    }
}

int main() {
    struct EncryptionData data;
    strcpy(data.message, "Hola IUA es viernes");
    strcpy(data.key, "10");

    printf("Mensaje original: %s\n", data.message);

    // Encriptar el mensaje
    encryptDecrypt(&data);
    printf("Mensaje encriptado: %s\n", data.message);

    // Desencriptar el mensaje
    encryptDecrypt(&data);
    printf("Mensaje desencriptado: %s\n", data.message);

    return 0;
}
