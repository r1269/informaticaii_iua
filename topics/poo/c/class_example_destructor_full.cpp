#include <iostream>
using namespace std;
class Fecha
{
    private:
        int mes;
        int dia;
        int anio;
    public:
        void setFecha (int, int, int);
        void printFecha(void);
        Fecha(int, int, int);
        ~Fecha();
};



void Fecha::setFecha(int d, int m, int a)
{
    mes =  m;
    dia =  d,
    anio = a;
}

void Fecha::printFecha()
{
    cout << "La fecha es " << dia << "/" << mes << "/" << anio<< endl;
}



Fecha::Fecha(int d, int m, int a)
{
    mes =  m;
    dia =  d,
    anio = a;
    cout << "Se creo un objeto nuevo. "<< endl;
    cout << "Con datos: " << dia <<
    "/" << mes << "/" << anio<< endl;
}

Fecha::~Fecha()
{
    cout << "Ejecutando el destructor "<< endl;
}



int main()
{
    Fecha f1(30,10,2022);
    f1.printFecha();
    Fecha f2(10,7,2022);
    f2.printFecha();
    return 0;
}
