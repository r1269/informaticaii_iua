#include <iostream>
using namespace std;
class my_class
{   private:
        int data;
    public:
        void setData (int);
        void printData(void);
};
void my_class::setData(int d)
{
    data=d;
}
void my_class::printData(void)
{
    cout <<"Data vale "<<data<< endl;
}







int main(void)
{
    my_class a;
    my_class *p;
    a.setData(1234);
    a.printData();
    p=&a;
    p->printData();
    p->setData(12);
    p->printData();
    
    return(0);
}
