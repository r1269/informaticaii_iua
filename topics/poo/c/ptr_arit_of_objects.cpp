#include <iostream>
using namespace std;
class my_class
{   private:
        int data;
    public:
        void setData (int);
        void printData(void);
};

void my_class::setData(int d)
{
    data=d;
}

void my_class::printData(void)
{
    cout <<"Data vale "<<data<< endl;
}





int main(void)
{
    my_class a[10];
    my_class *p;
    for(int ii=0;ii<10;ii++)
    {
        a[ii].setData(ii);
    }
    p=a;
    for(int ii=0;ii<10;ii++)
    {
        (p+ii)->printData();
    }
    return(0);
}
