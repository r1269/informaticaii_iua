#include <iostream>
using namespace std;
class my_class
{
    private:
        int data;
    public:
        void setData (int);
        void printData(void);
        ~my_class();
        my_class(int);
};



my_class::my_class(int d)
{
    data=d;
    cout <<"Construyendo"<< endl;
}


my_class:: ~my_class()
{
    cout <<"Destruyendo"<< endl;
}




void my_class::setData(int d)
{
    data=d;
}

void my_class::printData(void)
{
    cout <<"Data vale"<< endl;
    cout <<data<< endl;
}


void function(my_class);



int main()
{
    my_class a(10);
    a.printData();
    function(a);
    a.printData();
}







void function(my_class obj)
{
    cout <<"En la funcion"<< endl;
    obj.printData();
    obj.setData(13);
    obj.printData();
}

