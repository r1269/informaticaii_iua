#include <iostream>  
using namespace std;
class Fecha
{
    private:
        int mes;
        int dia;
        int anio;
    public:
        void setFecha (int, int, int);
        void printFecha(void);
};

void Fecha::setFecha(int d, int m, int a)
{
    mes =  m;
    dia =  d,
    anio = a;
}

void Fecha::printFecha()
{
    cout << "La fecha es " << dia << "/" << mes << "/" << anio<< endl; 
}


int main() 
{  
    Fecha f1;
    f1.setFecha(30,10,2022);
    f1.printFecha();
    return 0;  
}  