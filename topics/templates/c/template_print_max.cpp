#include <iostream>
using namespace std;

template <typename T> T Pmax(T x, T y)
{
    if(x>y)
    {
        return(x);
    }
    else
    {
        return(y);
    }
}
  
int main()
{
    cout << Pmax<int>(30, -10) << endl; 
    cout << Pmax<double>(3.1, 7.0)<< endl;
    cout << Pmax<char>('d', 'a')<< endl;
    return 0;
}