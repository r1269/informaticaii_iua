#include <iostream>
using namespace std;

template <class T> void swap(T *num1, T *num2)
{
    T temp;
    temp = *num1;
    *num1 = *num2;
    *num2 = temp;

}
  
int main()
{
    int i=10, j=20;
    double k=50.30, l=90.12;

    cout <<"Original i j "<< i << " " << j<<endl;
    swap(&i,&j);
    cout <<"Intercambiado i j "<< i << " " << j<<endl;
    cout <<"Original k l " << k << " " << l<<endl;
    swap(&k,&l);
    cout <<"Intercambiado k l "<< k << " " << l <<endl;
    return 0;
}