#include <stdio.h>
struct measurement
{
    float temp;
    float hum;
    int id;
};
/*Prototipo de la funcion*/
void print_members(struct measurement *);



void main()
{  
    struct measurement m;
    struct measurement *p;

    printf("Ingrese la temperatura\n");
    scanf("%f",&m.temp);
    printf("Ingrese la humedad\n");
    scanf("%f",&m.hum);
    m.id=10;
    p=&m;
    /*Llamada a la funcion*/
    print_members(p);
}


void print_members(struct measurement *m)
{
    printf("Temperatura \t %f\n",m->temp);
    printf("Humedad \t %f\n",m->hum);
    printf("Id \t \t %d\n",m->id);
}
