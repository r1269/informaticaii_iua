#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Definimos la estructura 'medicion'
struct medicion
{
    float temp;
    float hum;
    int id;
};

int main()
{
    // Inicializamos el generador de números aleatorios con la hora actual
    srand(time(NULL));

    // Creamos un arreglo de tres elementos de la estructura 'medicion'
    struct medicion mediciones[3];

    // Llenamos el arreglo con datos aleatorios utilizando un puntero
    struct medicion *ptr = mediciones; // Inicializamos el puntero

    for (int i = 0; i < 3; i++)
    {
        ptr->temp = (float)(rand() % 100) + 1.0; // Temperatura aleatoria entre 1.0 y 100.0
        ptr->hum = (float)(rand() % 100) + 1.0;  // Humedad aleatoria entre 1.0 y 100.0
        ptr->id = i + 1;                          // ID de 1 a 3
        ptr++;                                    // Movemos el puntero al siguiente elemento
    }

    // Imprimimos la información utilizando el puntero y el operador sizeof
    printf("Imprimiendo mediciones usando aritmética de punteros y sizeof:\n");
    ptr = mediciones; // Reiniciamos el puntero al principio del arreglo

    for (int i = 0; i < 3; i++)
    {
        printf("Medición %d:\n", ptr->id);
        printf("Temperatura: %.2f\n", ptr->temp);
        printf("Humedad: %.2f\n", ptr->hum);
        printf("Dirección: %p\n", ptr); // Imprimimos la dirección del elemento actual
        printf("\n");

        ptr++; // Movemos el puntero al siguiente elemento
    }

    return 0;
}
