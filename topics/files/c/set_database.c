    #include <stdio.h>  
    int main()  
    {  
        FILE *fptr;  
        int id;
        int ii;
        char nombre[30];  
        float sueldo;  
        fptr = fopen("database.txt", "w+");
        if (fptr == NULL)  
        {  
          printf("El archivo no existe \n");  
          return(1);  
        } 

        for(ii=0;ii<3;ii++)
        {
          scanf("%d", &id);  
          scanf("%s", nombre);  
          scanf("%f", &sueldo);  
          fprintf(fptr, "%d %s %f\n",id,nombre,sueldo);  
        }
        
        fclose(fptr);
        return(0);
    }  