\babel@toc {british}{}
\beamer@sectionintoc {1}{Secuencias y archivos}{2}{0}{1}
\beamer@sectionintoc {2}{El puntero a archivo}{4}{0}{2}
\beamer@sectionintoc {3}{Escritura y lectura de caracteres}{5}{0}{3}
\beamer@sectionintoc {4}{Escritura y lectura de cadenas}{16}{0}{4}
\beamer@sectionintoc {5}{Eliminación de archivos}{19}{0}{5}
\beamer@sectionintoc {6}{Leyendo y escribiendo datos de más de un byte}{20}{0}{6}
