#include <stdio.h>  
#include <stdlib.h>  

struct node 
{
  int numero;
  struct node *siguiente;
};

/*Prototipo de funciones*/
void push(struct node **, int);
void pop(struct node **);
void print(struct node **);
int isempty(struct node *);
int count_nodes(struct node **);

void menu(void)
{
    printf("1- Agregar\n2.- Eliminar\n");
    printf("3- Imprimir\n4- Contar nodos\n\n");
    printf("-1.- Salir\n");
}








void push (struct node **sp, int dato)
{
    struct node *new_node = NULL;
    new_node = malloc(sizeof(struct node));
    new_node = (struct node *) new_node;
    if(new_node==NULL)
     {   printf("No Memory available\n");
         exit(0);
     }
    new_node->numero = dato;
    new_node->siguiente = *(sp);
    *(sp) = new_node;
}


int isempty (struct node *sp)
{
    if(sp==NULL)
        return(1);
    else
        return(0);
}
void pop (struct node **sp)
{
    struct node *temp=NULL;
    temp = *(sp);
    **(sp) = *(*sp)->siguiente;
    //*(sp) = (*sp)->siguiente;
    free(temp);
}

void print (struct node **sp)
{
    struct node *temp;
    temp = *(sp);
    while (temp != NULL) 
    {
        printf("%d\n", temp->numero);
        temp = temp->siguiente;
    }
}




int count_nodes (struct node **sp)
{
    int acum=0;
    struct node *temp;
    temp = (*sp);
    while (temp != NULL) 
    {
        acum+=1;
        temp = temp->siguiente;
    }
    return(acum);
}



int main() 
{
    int op=0;
    int numero;
    struct node *stackptr = NULL;
    struct node *temp;

    while (op != -1) 
    {
    menu();
    scanf("%d", &op);
    switch (op) 
    {
    
    
    case 1:
        printf("Ingresa el numero a agregar:\n");
        scanf("%d", &numero);
        push(&stackptr,numero);
        break;
    case 2:
        if(isempty(stackptr)==0)
        {
            pop(&stackptr);
        }
        else
        {
            printf("Pila vacia");
        }
        break;
    case 3:
        printf("Imprimiendo...\n");
        print(&stackptr);
        break;
    case 4:
        printf("Nodos activos");
        printf("%d\n\n",count_nodes(&stackptr));

        break;
    }
  }
  return(0);
}





