#include <stdio.h>
#include "./math_lib/math_lib.h"  // Ruta relativa al archivo de encabezado
#include "./math_lib/math_lib.h"  // Ruta relativa al archivo de encabezado
#include "./math_lib/math_lib.h"  // Ruta relativa al archivo de encabezado

int main() {
    int num1 = 10, num2 = 5;
    printf("Suma: %d\n", suma(num1, num2)); 
    printf("Resta: %d\n", resta(num1, num2));
    printf("Multiplicacion: %d\n", multiplicacion(num1, num2));
    
    if (num2 != 0) {
        printf("Division: %.2f\n", division(num1, num2));
    } else {
        printf("Division por cero no permitida.\n");
    }
    
    return 0;
}
