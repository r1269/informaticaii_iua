#include <stdio.h>
void funcion() 
{
    static int contador = 0; // Variable estatica local
    contador++;
    printf("Contador: %d\n", contador);
}
int main (void)
{
    funcion();
    funcion();
    funcion();
    funcion();
    return(0);
}