#include <stdio.h>

int main(void)
{
    int valor = 100;
    const int *ptr = &valor;
    // (*ptr) = 50; // Esto generara un error
    int const *ptr2 = &valor; // Sintaxis alternativa

}
