#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 5

typedef struct 
{
    int *items;
    int *front; // Puntero al frente de la cola
    int *rear;  // Puntero al final de la cola
} CircularQueue;

void initializeQueue(CircularQueue *queue) 
{
    queue->items = (int *)malloc(MAX_SIZE * sizeof(int));
    queue->front = NULL; // Inicialmente, el frente y el final son nulos
    queue->rear = NULL;
}

int isFull(CircularQueue *queue) 
{
    if((queue->front == queue->items) && (queue->rear == queue->items + MAX_SIZE - 1))
        return(1);
    if((queue->rear == queue->front - 1)
        return(1);
    
           
    return(0);
}

int isEmpty(CircularQueue *queue) 
{
    return queue->front == NULL;
}

void enqueue(CircularQueue *queue, int value) 
{
    if (isFull(queue)) 
    {
        printf("La cola está llena. No se puede agregar más elementos.\n");
    } else 
    {
        if (isEmpty(queue)) 
        {
            queue->front = queue->items;
            queue->rear = queue->items;
        } else 
        {
            queue->rear++;
            if (queue->rear == queue->items + MAX_SIZE) 
            {
                // Si hemos llegado al final del arreglo, volvemos al principio
                queue->rear = queue->items;
            }
        }
        *(queue->rear) = value; // Almacenar el nuevo elemento en la cola
        printf("Elemento agregado: %d\n", value);
    }
}

int dequeue(CircularQueue *queue) 
{
    int item;
    if (isEmpty(queue)) 
    {
        printf("La cola está vacía. No se puede quitar ningún elemento.\n");
        return -1;
    } else 
    {
        item = *(queue->front); // Obtener el elemento del frente de la cola
        if (queue->front == queue->rear) 
        {
            queue->front = NULL; // Si solo hay un elemento en la cola, reiniciar los punteros
            queue->rear = NULL;
        } else 
        {
            queue->front++;
            if (queue->front == queue->items + MAX_SIZE) 
            {
                // Si hemos llegado al final del arreglo, volvemos al principio
                queue->front = queue->items;
            }
        }
        printf("Elemento eliminado: %d\n", item);
        return item;
    }
}

void freeQueue(CircularQueue *queue) 
{
    free(queue->items);
}

int main() 
{
    CircularQueue queue;
    initializeQueue(&queue);

    enqueue(&queue, 1);
    enqueue(&queue, 2);
    enqueue(&queue, 3);
    enqueue(&queue, 4);
    enqueue(&queue, 5);

    dequeue(&queue);
    dequeue(&queue);

    enqueue(&queue, 6);

    freeQueue(&queue);

    return 0;
}
