# include <stdio.h>
# include <stdlib.h>

/*Definicion de la esctructura*/
struct node
{
    int data;
    struct node *link;
};

int menu (void);
void push(struct node **,struct node **, int );
void pop(struct node **,struct node **);
void print(struct node *);

int menu (void)
{
    int op;
    do
    {
        printf("1-Agregar un nodo\n");
        printf("2-Borrar un nodo\n");
        printf("3-Imprimir cola\n");
        printf("4-Salir\n");
        scanf("%d",&op);
    }while((op<1)||(op>4));
    return(op);
}


void push(struct node **front,struct node **back, int d)
{
    struct node *temp;
    temp=(struct node *)malloc(sizeof(struct node));
    if(temp==NULL)
    {   printf("No Memory available\n");
        exit(0);
    }
    temp->data = d;
    temp->link=NULL;
    if(*back == NULL)  /*Insercion del primer nodo*/
    {
        *back = temp;
        *front = *back;
    }
    else /*Insercion del resto de los nodo*/
    {
        (*back)->link = temp;
        *back = temp;
    }
}

void pop(struct node **front,struct node **back)
{
    struct node *temp;
    if((*front == *back) && (*back == NULL))
    {
        printf("Vacia\n");
        exit(0);
    }
    temp = *front;
    *front = (*front)->link;
    if(*back == temp)
    {
        *back = (*back)->link;
    }

    free(temp);
}






void print(struct node *front)
{
    struct node *temp   =NULL;
    /*Impresion de toda la FIFO*/
    temp=front;
    while(temp!=NULL)
    {
        printf("%d->",temp->data);
        temp=temp->link;
    }
    printf("\n");
}



void main()
{
    struct node *front  =NULL;
    struct node *back   = NULL;
    struct node *temp   =NULL;
    int dato;
    int value;
    int op;
    do
    {   op=menu();
        switch(op)
        {
            case 1:
                printf("Ingrese el dato a insertar\n");
                scanf("%d",&dato);
                push(&front,&back,dato);
                break;
            case 2:
                pop(&front,&back);
                break;
            case 3:
                print(front);
                break;
        }
    }while(op!=4);

}
