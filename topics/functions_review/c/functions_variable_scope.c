/* Variables Locales*/
#include <stdio.h>
void func2(void);

int main () 
{
  /* Variables locales a Main */
  int a=10, b=20;
  printf ("Impresion en main\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
  func2();
  printf ("Impresion en main\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
  return 0;
}

void func2 () 
{
  /* Variables locales a func2 */
  int a=70, b=89;
  a++;
  b++;
  printf ("Impresion en Func2\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
}